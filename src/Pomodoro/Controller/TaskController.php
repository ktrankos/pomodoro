<?php
namespace Pomodoro\Controller;

/**
 * Description of SiteController
 *
 * @author watson
 * @date   17-sep-2014
 */
use Ats\MePhp;
use Spot\Config;
use RedBeanPHP\Facade;


class TaskController {

    

    
    public function Save()
    {     

    	$data = json_decode(file_get_contents('php://input'));
    	$task = Facade::dispense('tasks');
    	$task->name = $data->name;
    	$now = new \DateTime(); 

    	$task->created_at = $now->format('Y-m-d H:i:s');
    	$end = $now->modify('+30 minutes');
    	$id =  Facade::store($task);
    	if($id==FALSE)
    	{
    		echo json_encode(array('status'=>false));
    		return false;
    	}
    	$arr = array(
    		'status'=>true,
    		'task'=>array(
    			'id'=>$id,
    			'name'=>$data->name,
    			'created_at'=>$now,
    			'play'=>TRUE,
    			'timer'=>FALSE,
    			'pause'=>FALSE,
    			'finish'=>FALSE,
    			'doplay'=>''

    		)
    	); 
    	echo json_encode($arr);
    }
    public function Start()
    {     

    	$data = json_decode(file_get_contents('php://input'));
    	$task = Facade::load('tasks', $data->id);
    	$now = new \DateTime(); 
    	$task->start_at = $now->format('Y-m-d H:i:s');
    	$end = $now->modify('+30 minutes');

    	
    	$id =  Facade::store($task);
    	if($id==FALSE)
    	{
    		echo json_encode(array('status'=>false));
    		return false;
    	}
    	$arr = array(
    		'status'=>true,
    		'start_at'=>$end->format('Y/m/d H:i:s')    		
    	); 
    	echo json_encode($arr);
    }
}
