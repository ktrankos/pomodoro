<?php
namespace Pomodoro\Controller;

/**
 * Description of SiteController
 *
 * @author watson
 * @date   17-sep-2014
 */
use Ats\MePhp;
use Spot\Config;
use RedBeanPHP\Facade;


class SiteController {

    

    
    public function Home()
    {        
        $tpl = MePhp::$twig->loadTemplate("home.html.twig" );
        $query = Facade::findAll('tasks',
        ' ORDER BY id ASC ');
        $now = new \DateTime();
        $tasks = array();
        
        foreach ($query as $el) 
        {
        	$clase = 'list-group-item-info';
        	$started = FALSE;$finish = FALSE;
        	if($el->start_at!=NULL)
        	{
        		$started = new \DateTime($el->start_at);
        		$start_at = new \DateTime($el->start_at);
        		$play_at = $start_at->modify('+30 minutes');
        		$finish = ($now>$play_at);
        		$clase = ($finish)?'list-group-item-success': $clase;
        	}

        	$created = new \DateTime($el->created_at);
        	$end = new \DateTime($el->created_at);
        	$end->modify('+30 minutes');

            $tasks[]=array(
                'id'=>$el->id,                
                'name'=>$el->name,
                'created_at'=>$created->format('Y/m/d H:i:s'),
                'end_at'=>($now>$end)?'void': $end->format('Y/m/d H:i:s'),
                'clase'=>$clase,
                'play'=>($started==FALSE && !$finish),
                'timer'=>($started!=FALSE && !$finish),
                'pause'=>FALSE,
                'finish'=>$finish,
                'doplay'=>($started!=FALSE && !$finish) ? $play_at->format('Y/m/d H:i:s'):'',
            ); 
        }
        echo $tpl->render( array('tasks'=>$tasks));
    }
}
?>
