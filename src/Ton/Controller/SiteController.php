<?php
namespace Ton\Controller;

/**
 * Description of SiteController
 *
 * @author watson
 * @date   17-sep-2014
 */
use Ats\MePhp;
use Spot\Config;
use RedBeanPHP\Facade;
class SiteController {

    public function tpl()
    {
        
        
        
    }

    public function Login()
    {

        $tpl = MePhp::$twig->loadTemplate("login.html.twig" );
        echo $tpl->render( array());        
    }
    public function MakeLogin()
    {
        $user  = Facade::findOne( 'users', ' user = :name and pwd = :pwd', array('name'=>$_POST['user'], 'pwd'=>md5($_POST['pwd'])));


        if($user==NULL)
        {            
            $tpl = MePhp::$twig->loadTemplate("login.html.twig" );
            echo $tpl->render( array('error'=>"no existe ese usuario"));

        }else{
            $_SESSION['loged']= array('id'=>$user->getId());
            header('Location:/listado');
        }
        
    }
    public function HomePanel()
    {
        
        if(!isset($_SESSION['loged']))  header('Location: /');
        $user  = Facade::findOne( 'users', 'id=?' ,array($_SESSION['loged']['id']));
        
        if($user==NULL)  header('Location: /');
        $tpl = MePhp::$twig->loadTemplate("home.listado.html.twig" );
       
        echo $tpl->render( array('name'=>$user->user));
    }
}
?>
