<?php
namespace Ton\Entity;

/**
 * Description of User
 *
 * @author watson
 * @date   17-sep-2014
 */
class User extends \Spot\Entity
{
    protected static $table = 'users';

    public static function fields()
    {
        return array(
            'id' => array('type' => 'integer', 'primary' => true, 'autoincrement' => true),
            'pwd' => array('type' => 'text', 'required' => true),
            'name' => array('type' => 'text', 'required' => true),
            
        );
    }
}
?>
