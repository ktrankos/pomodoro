<?php
namespace Ats;

/**
 * Description of MePhp
 *
 * @author watson
 * @date   17-sep-2014
 */

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;
use RedBeanPHP\Facade;


class MePhp
{

    public static $twig;
    
    public function __construct($twig)
    {
        Facade::setup('mysql:host=localhost;dbname=db_pomodoro','root','');
        MePhp::$twig=$twig;
        $collection = new RouteCollection();
        
        $collection->attach(new Route('/', array(
            '_controller' => 'Pomodoro\Controller\SiteController::Home',
            'methods' => 'GET'
        ))); 
        $collection->attach(new Route('save', array(
            '_controller' => 'Pomodoro\Controller\TaskController::Save',
            'methods' => 'POST'
        )));    
        $collection->attach(new Route('start', array(
            '_controller' => 'Pomodoro\Controller\TaskController::Start',
            'methods' => 'POST'
        )));    
        $router = new Router($collection);
        $router->setBasePath('/');
        $route = $router->matchCurrentRequest();
    }
}
?>
