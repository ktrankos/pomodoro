<?php
    session_start();
	require __DIR__.'/../vendor/SplClassLoader.php';
    require __DIR__.'/../vendor/Twig/lib/Twig/Autoloader.php';
        

        Twig_Autoloader::register();
        $loader = new Twig_Loader_Filesystem(__DIR__.'/../src/Pomodoro/view/');
        $twig = new Twig_Environment($loader, array(
            'cache' => FALSE,
        ));
        
        
        $Atsloader = new SplClassLoader('Ats', __DIR__.'/../vendor');
        $Atsloader->register();

        $Routeloader = new SplClassLoader('PHPRouter', __DIR__.'/../vendor/Router/src');
        $Routeloader->register();

        $Tonloader = new SplClassLoader('Pomodoro', __DIR__.'/../src');
        $Tonloader->register();


        $RedBeanPHPloader = new SplClassLoader('RedBeanPHP', __DIR__.'/../vendor');
        $RedBeanPHPloader->register();
        



        use Ats\MePhp;
        ///init mcvc
        new MePhp($twig);
        
?>