var pomodoroApp = angular.module('pomodoroApp', []);

pomodoroApp.controller('siteCtrl', function ($scope) {
  	$scope.tasks = _tasks;
});
pomodoroApp.controller('mainMenuCtrl', function ($scope) 
{
	$scope.createNew = function(e){
		e.preventDefault();
		$('#formCreate').modal({});

	}
  
});
pomodoroApp.controller('listadoCtrl', function ($scope, $http) 
{
	
	

	
  	$scope.saveTask=function(e, task)
  	{
  		e.preventDefault();
  		if($scope.newtask.$valid)
  		{  			
  			$http.post($(e.currentTarget).attr('action'), task).
			  success(function(data, status, headers, config) {
			  	if(data.status) {
			  		$play = true; $pause = false; $clase = 'list-group-item-info';
			  		angular.forEach($scope.$parent.tasks, function(task) {
						if(task.timer) {
							$play = false;
							$pause = true;
							$clase = 'list-group-item-warning';
						}
					});
					data.task.play = $play;
					data.task.pause = $pause;
					data.task.clase = $clase;
			  		$scope.$parent.tasks.push(data.task);
			  	}
			  			    	
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			  });
  		}
  		return false;
  	}
});
pomodoroApp.controller('taskNewCtrl', function ($scope, $http) {
  	$scope.saveTask=function(e, task)
  	{
  		e.preventDefault();
  		if($scope.newtask.$valid){
  			console.log(task);
  		}
  		return false;
  	}
});
pomodoroApp.directive('atsPomodoro', function($interval, $http) {
	function link(scope, element, attrs) 
	{
	    var seconds, _end;
	    if(attrs.taskplay.length>0)
	    {
	    	startTimer(attrs.taskplay);
	    }
	    scope.startTask = function(e)
		{
			$playing = false;
			angular.forEach(scope.$parent.tasks, function(task) {
				if(task.timer) $playing = true;
			});
			
			if($playing || scope.$parent.tasks[attrs.id].finish) return false;
			$http.post('/start', {id: attrs.taskid})
			.success(function(data, status, headers, config) {
			  	if(data.status)
			  	{
			  		startTimer(data.start_at);
			  	}			  	
			  })
			.error(function(data, status, headers, config) {			    
			});

		}
		//bloqueamos las restantes
		function updateStatus(el, end)
		{			
			var end =(typeof(end)=='undefined') ? false: true;
			
			angular.forEach(scope.$parent.tasks, function(task) {
				
				if(task.id!=el.id)
				{
					task.play = (!task.finish);
					
	    			task.timer = false;
	    			task.clase = (!task.finish) ? 'list-group-item-warning':'list-group-item-success';
	    			if(!task.finish && end) task.clase = 'list-group-item-info';
	    			task.pause = (!task.finish && !end)? true : false; 	
				}
	    		
	    	});
		}
	    function updateTime()
	    {
	    	if(attrs.taskend=='void')
	    	{
	    		scope.$parent.clase = 'list-group-item-success';
	    		$interval.cancel(timeoutId);
	    		element.text('finalizado');
	    		return false;
	    	}

	    	
	    	seconds--;
	    	if(seconds<=30) scope.$parent.tasks[attrs.id].clase='list-group-item-danger';
	    	
	    	if(seconds<=0)
	    	{
	    		updateStatus(scope.$parent.tasks[attrs.id], true);
	    		scope.$parent.tasks[attrs.id].clase='list-group-item-success';
	    		scope.$parent.tasks[attrs.id].timer = false;
	    		scope.$parent.tasks[attrs.id].finish = true;

	    		$interval.cancel(timeoutId);
	    		element.text('');
	    		return false;
	    	}
	    	_minutes = parseInt( (seconds / 60 ) % 60);
			_seconds = parseInt(seconds % 60);	    	
	    	_m =(_minutes<=9) ? '0'+_minutes:_minutes;
	    	_s = (_seconds<=9) ? '0'+_seconds:_seconds;

	    	element.text(_m+":"+_s);
	    }
	    function startTimer(date)
	    {
	    	updateStatus(scope.$parent.tasks[attrs.id]);
	    	
	    	/*/////*/
	    	scope.$parent.tasks[attrs.id].play = false;
	    	scope.$parent.tasks[attrs.id].timer = true;
	    	scope.$parent.tasks[attrs.id].clase = 'list-group-item-info';
	    	
			_end = new Date(date);
			now = new Date();
			seconds = (_end.getTime() - now.getTime())/1000;
			if(seconds<=30) scope.$parent.tasks[attrs.id].clase='list-group-item-danger';
			_minutes = parseInt( (seconds / 60 ) % 60);
			_seconds = parseInt(seconds % 60);
			    	
			_m =(_minutes<=9) ? '0'+_minutes:_minutes;
			_s = (_seconds<=9) ? '0'+_seconds:_seconds;
			element.text(_m+":"+_s);
			///intervalo
	    	timeoutId = $interval(function() {
		      updateTime(); // update DOM
		    }, 1000);	
	    }
	    
  }
  return {
  	link:link
  }  
});

pomodoroApp.directive('atsTimer', function($interval) {
	function link(scope, element, attrs) 
	{
	    var seconds, _end;
	    element.text('finalizado');
	    
	    if(attrs.taskend!='void')
	    {
	    	_end = new Date(attrs.taskend);
	    	now = new Date();
	    	seconds = (_end.getTime() - now.getTime())/1000;
	    	_minutes = parseInt( (seconds / 60 ) % 60);
			_seconds = parseInt(seconds % 60);
	    	
	    	_m =(_minutes<=9) ? '0'+_minutes:_minutes;
	    	_s = (_seconds<=9) ? '0'+_seconds:_seconds;
	    	element.text(_m+":"+_s);
	    	
	    }
	    

	    function updateTime()
	    {
	    	if(attrs.taskend=='void')
	    	{
	    		scope.$parent.clase = 'list-group-item-success';
	    		$interval.cancel(timeoutId);
	    		element.text('finalizado');
	    		return false;
	    	}

	    	
	    	seconds--;
	    	if(seconds<=30)
	    	{
	    		
	    		scope.$parent.tasks[attrs.id].clase='list-group-item-danger';
	    	}
	    	if(seconds<=0)
	    	{
	    		scope.$parent.tasks[attrs.id].clase='list-group-item-success';
	    		$interval.cancel(timeoutId);
	    		element.text('finalizado');
	    		return false;
	    	}
	    	_minutes = parseInt( (seconds / 60 ) % 60);
			_seconds = parseInt(seconds % 60);	    	
	    	_m =(_minutes<=9) ? '0'+_minutes:_minutes;
	    	_s = (_seconds<=9) ? '0'+_seconds:_seconds;

	    	element.text(_m+":"+_s);
	    }

	    timeoutId = $interval(function() {
	      updateTime(); // update DOM
	    }, 1000);
  }
  return {
  	link:link
  }  
});

